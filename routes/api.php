<?php

use App\Http\Controllers\CharacterController;
use App\Http\Controllers\PlanetController;
use App\Http\Controllers\SpeciesController;
use App\Http\Controllers\StarshipController;
use App\Http\Controllers\VehicleController;
use App\Models\Character;
use App\Models\Planet;
use App\Models\Species;
use App\Models\Starship;
use App\Models\Vehicle;
use App\Search\SWAPI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('character', CharacterController::class)->only(["index", "show"]);
Route::apiResource('planet', PlanetController::class)->only(["index", "show"]);
Route::apiResource('species', SpeciesController::class)->only(["index", "show"]);
Route::apiResource('starship', StarshipController::class)->only(["index", "show"]);
Route::apiResource('vehicle', VehicleController::class)->only(["index", "show"]);


Route::get('/searchType/{resource}/{searchString}', function (Request $request, $resource, $searchString) {
    $modelLinks = [
        'character' => Character::class,
        'planet' => Planet::class,
        'species' => Species::class,
        'starship' => Starship::class,
        'vehicle' => Vehicle::class
    ];

    try {
        return $modelLinks[$resource]::search($searchString)->get();
    } catch (Throwable $e) {
        abort(404);
    }
});

Route::get('/search/{searchString}', function (Request $request, $searchString) {
    try {
        return SWAPI::search($searchString)->get();
    } catch (Throwable $e) {
        abort(404);
    }
});