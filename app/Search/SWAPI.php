<?php

namespace App\Search;

use Algolia\ScoutExtended\Searchable\Aggregator;

class SWAPI extends Aggregator
{
    /**
     * The names of the models that should be aggregated.
     *
     * @var string[]
     */
    protected $models = [
        \App\Models\Character::class,
        \App\Models\Planet::class,
        \App\Models\Species::class,
        \App\Models\Starship::class,
        \App\Models\Vehicle::class,
    ];
}
