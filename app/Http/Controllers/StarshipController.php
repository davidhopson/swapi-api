<?php

namespace App\Http\Controllers;

use App\Models\Starship;

class StarshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Starship::paginate(15);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Starship  $starship
     * @return \Illuminate\Http\Response
     */
    public function show(Starship $starship)
    {
        return $starship;
    }
}
